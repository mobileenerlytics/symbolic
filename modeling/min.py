#!/usr/local/bin/python3
from scipy.optimize import minimize
import numpy as np


idx_Screen_base = 0
idx_Screen_max = 1
idx_GPU_active_240000000 = 2
idx_GPU_active_300000000 = 3
idx_GPU_active_389000000 = 4
idx_GPU_active_500000000 = 5
idx_GPU_active_600000000 = 6
idx_GPU_nap_240000000 = 7
idx_GPU_nap_300000000 = 8
idx_GPU_nap_389000000 = 9
idx_GPU_nap_500000000 = 10
idx_GPU_nap_600000000 = 11
idx_GPUbase = 12
idx_Core_active_300000 = 13
idx_Core_active_422400 = 14
idx_Core_active_652800 = 15
idx_Core_active_729600 = 16
idx_Core_active_883200 = 17
idx_Core_active_960000 = 18
idx_Core_active_1036800 = 19
idx_Core_active_1190400 = 20
idx_Core_active_1267200 = 21
idx_Core_active_1497600 = 22
idx_Core_active_1574400 = 23
idx_Core_active_1728000 = 24
idx_Core_active_1958400 = 25
idx_Core_active_2265600 = 26
idx_Core_active_2457600 = 27
idx_Core_active_2496000 = 28
idx_Core_active_2572800 = 29
idx_Core_active_2649600 = 30
idx_GPS_active = 31
idx_LAST = 32

# with open('/Users/abhilash/Downloads/traces/eagle-0813526070551/eprof.files.sym/lse','r') as f:
#     errorExpr = f.read()
# with open('/Users/abhilash/Downloads/traces/eagle-7940268060551/eprof.files.sym/lse','r') as f:
#     errorExpr = errorExpr + " + " + f.read()

with open('/Users/abhilash/Downloads/traces/eagle-0282050170551/eprof.files.sym/lse','r') as f:
    errorExpr = f.read()

def f(Screen_base, Screen_max,\
GPU_active_240000000,    GPU_active_300000000, GPU_active_389000000, GPU_active_500000000, GPU_active_600000000,\
GPU_nap_240000000, GPU_nap_300000000, GPU_nap_389000000, GPU_nap_500000000, GPU_nap_600000000,GPUbase,\
Core_active_300000, Core_active_422400, Core_active_652800, Core_active_729600, Core_active_883200, Core_active_960000,\
Core_active_1036800, Core_active_1190400, Core_active_1267200, Core_active_1497600, Core_active_1574400, Core_active_1728000,\
Core_active_1958400, Core_active_2265600, Core_active_2457600, Core_active_2496000, Core_active_2572800, Core_active_2649600, GPS_active):
    return eval(errorExpr)

def optimize(x):
    return f(Screen_base = x[idx_Screen_base],
    Screen_max = x[idx_Screen_max],
    GPU_active_240000000 = x[idx_GPU_active_240000000],
    GPU_active_300000000 = x[idx_GPU_active_300000000],
    GPU_active_389000000 = x[idx_GPU_active_389000000],
    GPU_active_500000000 = x[idx_GPU_active_500000000],
    GPU_active_600000000 = x[idx_GPU_active_600000000],
    GPU_nap_240000000 = x[idx_GPU_nap_240000000],
    GPU_nap_300000000 = x[idx_GPU_nap_300000000],
    GPU_nap_389000000 = x[idx_GPU_nap_389000000],
    GPU_nap_500000000 = x[idx_GPU_nap_500000000],
    GPU_nap_600000000 = x[idx_GPU_nap_600000000],
    GPUbase = x[idx_GPUbase],
    Core_active_300000 = x[idx_Core_active_300000],
    Core_active_422400 = x[idx_Core_active_422400],
    Core_active_652800 = x[idx_Core_active_652800],
    Core_active_729600 = x[idx_Core_active_729600],
    Core_active_883200 = x[idx_Core_active_883200],
    Core_active_960000 = x[idx_Core_active_960000],
    Core_active_1036800 = x[idx_Core_active_1036800],
    Core_active_1190400 = x[idx_Core_active_1190400],
    Core_active_1267200 = x[idx_Core_active_1267200],
    Core_active_1497600 = x[idx_Core_active_1497600],
    Core_active_1574400 = x[idx_Core_active_1574400],
    Core_active_1728000 = x[idx_Core_active_1728000],
    Core_active_1958400 = x[idx_Core_active_1958400],
    Core_active_2265600 = x[idx_Core_active_2265600],
    Core_active_2457600 = x[idx_Core_active_2457600],
    Core_active_2496000 = x[idx_Core_active_2496000],
    Core_active_2572800 = x[idx_Core_active_2572800],
    Core_active_2649600 = x[idx_Core_active_2649600],
    GPS_active = x[idx_GPS_active])

def main():


    x0 = np.array([None] * idx_LAST)
    
    x0[idx_Screen_base] = 170
    x0[idx_Screen_max] = 440
    x0[idx_GPU_active_240000000] = 147
    x0[idx_GPU_active_300000000] = 157
    x0[idx_GPU_active_389000000] = 187
    x0[idx_GPU_active_500000000] = 287
    x0[idx_GPU_active_600000000] = 377
    x0[idx_GPU_nap_240000000] = 87
    x0[idx_GPU_nap_300000000] = 107
    x0[idx_GPU_nap_389000000] = 117
    x0[idx_GPU_nap_500000000] = 187
    x0[idx_GPU_nap_600000000] = 237
    x0[idx_GPUbase] = 0
    x0[idx_Core_active_300000] =  19
    x0[idx_Core_active_422400] =  25
    x0[idx_Core_active_652800] =  37
    x0[idx_Core_active_729600] =  41
    x0[idx_Core_active_883200] =  48
    x0[idx_Core_active_960000] =  62
    x0[idx_Core_active_1036800] = 67
    x0[idx_Core_active_1190400] = 78
    x0[idx_Core_active_1267200] = 89
    x0[idx_Core_active_1497600] = 108
    x0[idx_Core_active_1574400] = 118
    x0[idx_Core_active_1728000] = 133
    x0[idx_Core_active_1958400] = 158
    x0[idx_Core_active_2265600] = 207
    x0[idx_Core_active_2457600] = 255
    x0[idx_Core_active_2496000] = 259
    x0[idx_Core_active_2572800] = 280
    x0[idx_Core_active_2649600] = 305
    x0[idx_GPS_active] = 15

    bds = [(10, 600)] * idx_LAST
    for b in range(idx_LAST) :
        bds[b] = (0.7 * x0[b], 1.3 * x0[b])     # plus/minus 30% of original values

    cons = []
    # for i in range(31):
    #     cons.append({'type' : 'ineq', 'fun': lambda x : x[i]})   # Non-negative

    # Screen max is higher than screen base
    cons.append({'type' : 'ineq', 'fun': lambda x : x[idx_Screen_max] - x[idx_Screen_base]})

    # GPU active current for higher GPU frequency should be higher
    for gpuactive in range(idx_GPU_active_240000000, idx_GPU_active_600000000) :
        cons.append({'type': 'ineq', 'fun': lambda x: x[gpuactive+1] - x[gpuactive]})

    # GPU nap current for higher GPU frequency should be higher
    for gpunap in range(idx_GPU_nap_240000000, idx_GPU_nap_600000000) :
        cons.append({'type': 'ineq', 'fun': lambda x: x[gpunap+1] - x[gpunap]})

    # GPU active current is higher than nap current in same frequency
    for gpufreq in range(idx_GPU_active_240000000, idx_GPU_active_600000000 + 1) :
        cons.append({'type': 'ineq', 'fun': lambda x:  x[gpufreq] - x[gpufreq + (idx_GPU_nap_600000000 - idx_GPU_active_600000000)]})

    # GPUbase is zero
    cons.append({'type': 'eq', 'fun': lambda x: x[idx_GPUbase]})

    # Core in higher frequency should consume higher energy
    for cpuactive in range(idx_Core_active_300000, idx_Core_active_2649600) :
        cons.append({'type': 'ineq', 'fun': lambda x: x[cpuactive+1] - x[cpuactive]})

    # print(cons)
    res = minimize(optimize, x0, method='SLSQP', bounds=bds, constraints=cons, options={'maxiter': 500})
    # res = minimize(optimize, x0, method='COBYLA', constraints=cons)
    # res = minimize(optimize, x0, method='trust-constr', bounds=bds, constraints=cons)
    print(res)
    if not res.success :
        print("Failed to optimize")
        return

    x = res.x
    print("Screen_base :", x[idx_Screen_base])
    print("Screen_max :", x[idx_Screen_max])
    print("GPU_active_240000000 :", x[idx_GPU_active_240000000])
    print("GPU_active_300000000 :", x[idx_GPU_active_300000000])
    print("GPU_active_389000000 :", x[idx_GPU_active_389000000])
    print("GPU_active_500000000 :", x[idx_GPU_active_500000000])
    print("GPU_active_600000000 :", x[idx_GPU_active_600000000])
    print("GPU_nap_240000000 :", x[idx_GPU_nap_240000000])
    print("GPU_nap_300000000 :", x[idx_GPU_nap_300000000])
    print("GPU_nap_389000000 :", x[idx_GPU_nap_389000000])
    print("GPU_nap_500000000 :", x[idx_GPU_nap_500000000])
    print("GPU_nap_600000000 :", x[idx_GPU_nap_600000000])
    print("GPUbase :", x[idx_GPUbase])
    print("Core_active_300000 :", x[idx_Core_active_300000])
    print("Core_active_422400 :", x[idx_Core_active_422400])
    print("Core_active_652800 :", x[idx_Core_active_652800])
    print("Core_active_729600 :", x[idx_Core_active_729600])
    print("Core_active_883200 :", x[idx_Core_active_883200])
    print("Core_active_960000 :", x[idx_Core_active_960000])
    print("Core_active_1036800 :", x[idx_Core_active_1036800])
    print("Core_active_1190400 :", x[idx_Core_active_1190400])
    print("Core_active_1267200 :", x[idx_Core_active_1267200])
    print("Core_active_1497600 :", x[idx_Core_active_1497600])
    print("Core_active_1574400 :", x[idx_Core_active_1574400])
    print("Core_active_1728000 :", x[idx_Core_active_1728000])
    print("Core_active_1958400 :", x[idx_Core_active_1958400])
    print("Core_active_2265600 :", x[idx_Core_active_2265600])
    print("Core_active_2457600 :", x[idx_Core_active_2457600])
    print("Core_active_2496000 :", x[idx_Core_active_2496000])
    print("Core_active_2572800 :", x[idx_Core_active_2572800])
    print("Core_active_2649600 :", x[idx_Core_active_2649600])
    print("GPS_active :", x[idx_GPS_active])

main()
