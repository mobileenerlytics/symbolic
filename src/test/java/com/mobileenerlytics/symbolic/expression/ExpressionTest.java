package com.mobileenerlytics.symbolic.expression;

import org.junit.Test;

import java.util.HashMap;

import static com.mobileenerlytics.symbolic.expression.Term.CONSTANT;
import static org.junit.Assert.assertEquals;

public class ExpressionTest {
    @Test
    public void testAdditions() {
        Expression first = new Addition(new HashMap<String, Term>() {{
            put("a", new Term(1, "a"));
            put("b", new Term(2, "b"));
            put(CONSTANT, new Term(3, CONSTANT));
            put("c", new Term(4, "c"));
            put("e", new Term(1, "e"));
        }});

        Expression second = new Addition(new HashMap<String, Term>() {{
            put("a", new Term(3, "a"));
            put("b", new Term(2, "b"));
            put(CONSTANT, new Term(1, CONSTANT));
            put("d", new Term(4, "d"));
            put("e", new Term(-1, "e"));
        }});


        Expression expected = new Addition(new HashMap<String, Term>() {{
            put("a", new Term(4, "a"));
            put("b", new Term(4, "b"));
            put(CONSTANT, new Term(4, CONSTANT));
            put("c", new Term(4, "c"));
            put("d", new Term(4, "d"));
        }});    // e term got cancelled out

        assertEquals(expected, Expression.add(first, second));
        assertEquals(expected, Expression.add(second, first));
    }

    @Test
    public void testTermAddition() {
        Expression first = new Term(4, "a");
        Expression second = new Term(3, "b");
        assertEquals("(4.0000 * a) + (3.0000 * b)", Expression.add(second, first).toString());
    }

    @Test
    public void testConstantTerms() {
        Expression first = new Term(4, CONSTANT);
        Expression second = new Term(3, CONSTANT);
        assertEquals(new Term(7, CONSTANT), Expression.add(second, first));
    }

    @Test
    public void testEvaluateConstant() {
        assertEquals(3.0, Expression.evaluate("(3.0)", null), 0.1);
    };

    @Test
    public void testEvaluateAddition() {
        assertEquals(53.0, Expression.evaluate("(1.0 * a) + (2.0 * b) + (3.0)", new HashMap<String, Double>() {{
            put("a", 10.0);
            put("b", 20.0);
        }}), 0.1);
    }
}
