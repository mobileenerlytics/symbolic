package com.mobileenerlytics.symbolic.expression;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TermTest {
    @Test
    public void testTermImmutableMultiply() {
        Term a = new Term(1, "a");
        Term b = (Term) a.multiply(3.0);

        assertEquals(1, a.value, 0.01); // a is immutable
        assertEquals(3, b.value, 0.01); // b is indeed multiply
        assertTrue(b.name.equals("a")); // variable name is correct for b
    }

    @Test
    public void testTermZero() {
        assertTrue((new Term(0, "a")).isZero());
        assertFalse((new Term(1, "a")).isZero());
    }

    @Test
    public void testTermNumber() {
        assertTrue((new Term(0, Term.CONSTANT)).isNumber());
        assertFalse((new Term(1, "a")).isNumber());
    }

    @Test
    public void testTermEquals() {
        assertFalse((new Term(3, "a")).equals(new Term(4, "a")));
        assertFalse((new Term(3, "a")).equals(new Term(3, "b")));
        assertTrue((new Term(3, "a")).equals(new Term(3, "a")));
    }
}
