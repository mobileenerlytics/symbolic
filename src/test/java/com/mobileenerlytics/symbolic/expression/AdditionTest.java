package com.mobileenerlytics.symbolic.expression;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AdditionTest {
    Addition addition;

    @Before
    public void setup() {
        // 3a + 4b
        addition = new Addition(new Term(4, "b"), new Term(3, "a"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddZero() {
        new Addition(new Term(0, "a"), new Term(4, "a"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddSame() {
        new Addition(new Term(3, "a"), new Term(4, "a"));
    }

    @Test
    public void multiplyZero() {
        assertTrue(((Term) addition.multiply(0)).isZero());
    }

    @Test
    public void multiplyConstant() {
        assertEquals("(9.0000 * a) + (12.0000 * b)", addition.multiply(3).toString());
    }

    @Test
    public void testToString() {
        assertEquals("(3.0000 * a) + (4.0000 * b)", addition.toString());
    }

    @Test
    public void testEquals() {
        assertFalse(addition.equals(new Addition(new HashMap<String, Term>() {{
            put("a", new Term(1, "a"));
            put("b", new Term(4, "b"));
        }})));

        assertTrue(addition.equals(new Addition(new HashMap<String, Term>() {{
            put("a", new Term(3, "a"));
            put("b", new Term(4, "b"));
        }})));
    }
}
