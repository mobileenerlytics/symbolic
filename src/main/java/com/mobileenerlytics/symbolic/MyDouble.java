package com.mobileenerlytics.symbolic;

import com.mobileenerlytics.symbolic.expression.Expression;

public abstract class MyDouble {
    public abstract MyDouble add(MyDouble md);

    public abstract MyDouble subtract(MyDouble md);

    public abstract MyDouble multiply(MyDouble md);

    public abstract MyDouble multiply(double value);

    public abstract MyDouble divide(double value);

    public abstract Expression toExpression();

    public boolean isUnmodifiable() {
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if(! (o instanceof MyDouble))
            return false;
        MyDouble md = (MyDouble) o;
        return toExpression().equals(md.toExpression());
    }
}
