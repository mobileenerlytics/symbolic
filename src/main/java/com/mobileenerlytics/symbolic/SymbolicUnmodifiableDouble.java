package com.mobileenerlytics.symbolic;

import com.mobileenerlytics.symbolic.expression.Expression;
import com.mobileenerlytics.symbolic.expression.Term;

class SymbolicUnmodifiableDouble extends MyDouble {
    private final Expression expression;
    public SymbolicUnmodifiableDouble(final double value, String variableName) {
        expression = new Term(value, variableName);
    }

    private SymbolicUnmodifiableDouble(Expression expression) {
        this.expression = expression;
    }

    public SymbolicUnmodifiableDouble(MyDouble myDouble) {
        expression = Expression.clone(myDouble.toExpression());
    }

    @Override
    public MyDouble add(final MyDouble operand) {
        return new SymbolicUnmodifiableDouble(Expression.add(expression, operand.toExpression()));
    }

    @Override
    public MyDouble subtract(final MyDouble operand) {
        return new SymbolicUnmodifiableDouble(Expression.subtract(expression, operand.toExpression()));
    }

    @Override
    public MyDouble multiply(MyDouble operand) {
        return new SymbolicUnmodifiableDouble(Expression.multiply(expression, operand.toExpression()));
    }

    @Override
    public MyDouble multiply(double value) {
        return new SymbolicUnmodifiableDouble(expression.multiply(value));
    }

    @Override
    public MyDouble divide(double value) {
        return new SymbolicUnmodifiableDouble(expression.multiply(1.0/value));
    }

    @Override
    public Expression toExpression() {
        return expression;
    }

    @Override
    public String toString() {
        return expression.toString();
    }


    @Override
    public boolean isUnmodifiable() {
        return true;
    }
}
