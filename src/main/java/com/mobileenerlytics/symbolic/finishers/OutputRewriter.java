package com.mobileenerlytics.symbolic.finishers;

import com.mobileenerlytics.symbolic.expression.Expression;
import com.mobileenerlytics.symbolic.expression.SymbolicBridge;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class plugs in variable values in a `variables.prop` file to the symbolic outputs in
 * {@link SymbolicBridge#getOutDir()}, dumped after running eprof, into the {@link SymbolicBridge#getConcreteOutDir()}
 *
 * @author abhilash
 */
@Slf4j
@Component
class OutputRewriter {
    @Autowired
    SymbolicBridge bridge;
//    SymbolicBridge bridge = new MockSymbolicBridge();

    final Pattern pattern = Pattern.compile("(\\(-?\\d+\\.\\d+( \\* [^ ]*)?\\) \\+ )*\\(-?\\d+\\.\\d+( \\* [^ ]*)?\\)");


    /**
     * Rewrite symbolic output file to concrete output files by plugging in variable values.
     *
     * Also, directly copy over other output files, such as screen.mp4, from symbolic output dir to concrete output dir
     * @throws IOException
     */
    void rewrite(Map<String, Double> values) throws IOException {
        File outDir = bridge.getOutDir().toFile();

        /** Filter out all csv and json files */
        Predicate<String> filter = name -> name.endsWith(".csv") || name.endsWith(".json");

        log.info("Rewriting symbolic files");
        /** Rewrite files found by the filter */
        File[] rewriteFiles = outDir.listFiles((dir, name) -> filter.test(name));
        for(File rewriteFile: rewriteFiles) {
            Iterable<String> iterable = Files.lines(rewriteFile.toPath()).map(s -> {
                Matcher matcher = pattern.matcher(s);
                if (!matcher.find())
                    return s;
                return matcher.replaceFirst(String.format("%.4f", Expression.evaluate(s, values)));
            })::iterator;

            Path outFile = bridge.getConcreteOutDir().resolve(rewriteFile.getName());
            Files.write(outFile, iterable);
        }

        log.info("Copying over other files");
        /** Directly copy over other files that are not caught by filter */
        File[] otherFiles = outDir.listFiles((dir, name) -> filter.negate().test(name));
        for(File otherFile : otherFiles) {
            Path outFile = bridge.getConcreteOutDir().resolve(otherFile.getName());
            Files.copy(otherFile.toPath(), outFile, StandardCopyOption.REPLACE_EXISTING);
        }
    }

//    static String parent = "/var/folders/7j/2jcnf3wj7s13tfcc_w1c8zh00000gq/T/5cb15901cd200aa2df8ffd8b-hash-J1-41833854041594976175d0bc951fcb8686917722f17-hash-Calendar-6.0-LightMode-7611883658753572967eagle-9688849661651/eagle-9688849661651/";
//    public class MockSymbolicBridge extends SymbolicBridge {
//        @Override
//        public Path getOutDir() {
//            return FileSystems.getDefault().getPath(parent + "eprof.files.sym/");
//        }
//
//        @Override
//        public Path getConcreteOutDir() {
//            return FileSystems.getDefault().getPath(parent + "eprof.files/");
//        }
//    }
//
//    public static void main(String[] args) throws IOException {
//        OutputRewriter outputRewriter = new OutputRewriter();
//        outputRewriter.rewrite(Optimizer.getVariableValues(
//                FileSystems.getDefault().getPath(parent + "eprof.files.sym/variables.prop")));
//    }
}
