package com.mobileenerlytics.symbolic.finishers;

import java.util.LinkedList;
import java.util.List;

import static java.util.stream.Collectors.joining;

/**
 * This holds a python array
 * @param <T>
 */
class PythonArray<T> {
    List<T> values = new LinkedList<>();

    @Override
    public String toString() {
        return "np.array([" + values.stream().map(v -> v.toString()).collect(joining(",")) + "])";
    }

    void add(T t) {
        values.add(t);
    }
}
