package com.mobileenerlytics.symbolic.finishers;

import com.mobileenerlytics.symbolic.DoubleFactory;
import com.mobileenerlytics.symbolic.IEvent;
import com.mobileenerlytics.symbolic.MyDouble;
import com.mobileenerlytics.symbolic.expression.Addition;
import com.mobileenerlytics.symbolic.expression.SymbolicBridge;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.*;

import static java.util.stream.Collectors.*;

/**
 * This class writes the least square error expression into `lse` file from reading `ERROR.csv` file.
 * Next it generates a python code which reads from `lse` file and minimizes it. The python output is written into
 * `variables.prop` file. It finally calls {@link OutputRewriter#rewrite(Map)} )} with model parameter values
 * rewrites symbolic output csv and json files.
 */
@Slf4j
@Component
public class Optimizer {
    /**
     * Aggregate samples from ERROR.csv in this many microseconds before creating least square error expression.
     * This is done to amortize the shift in time between current sensor and estimated power reading.
     * If the shift is 1 sample, ~200ms, we thus set default value to 2000 ms (200 * 10) to reduce the error
     * caused due to shift to less than 10%
     */
    @Value("${aggr_error_window_in_us:2000000}")
    int aggrErrorWindowSizeUs;

    @Value("${python3_path:python3}")
    String python3;

    @Autowired
    SymbolicBridge bridge;

    @Autowired
    OutputRewriter outputRewriter;

    public void optimizeAndRewrite(SortedMap<String, Double> initValues, List<? extends IEvent<MyDouble>> errorEvents) throws IOException, InterruptedException {
        if(errorEvents == null || errorEvents.isEmpty()) {
            log.warn("No error events, directly plugging in initial values");
            outputRewriter.rewrite(initValues);
            return;
        }

        /* Aggregate error by windows of size {@link #aggrErrorWindowSizeUs} */
        Map<Long, MyDouble> aggregatedErrorByWindow = errorEvents.stream()
                .collect(groupingBy(e -> e.getTimeUs() / aggrErrorWindowSizeUs, // Group error expressions by "window index"
                        mapping(IEvent::getValue,
                                reducing(DoubleFactory.newUnmodifiableDouble(0), MyDouble::add))));// Sum errors within a window

        LinearEquation linearEquation = createLinearEquation(initValues, aggregatedErrorByWindow);
        File pyFile = generatePyCode(initValues, linearEquation);

        log.info("\n\nRunning python code to minimize error\n\n");
        Path variablesProp = bridge.getOutDir().resolve("variables.prop");

        if(!FileSystems.getDefault().getPath(python3).toFile().exists()) {
            log.warn("No file found at the specified path to python3 {}. Defaulting to just python3", python3);
            python3 = "python3";
        }
        Process pyProc = new ProcessBuilder(python3, pyFile.getAbsolutePath())
                .redirectOutput(variablesProp.toFile())
                .start();

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(pyProc.getErrorStream()))) {
            String s;
            while ((s = bufferedReader.readLine()) != null) {
                log.info(s);
            }
            pyProc.waitFor();
        }
        log.info("\n\nFinished running python code\n\n");

        Map<String, Double> values = getVariableValues(variablesProp);
        outputRewriter.rewrite(values);
    }

    /**
     * Read `variables.prop` file into a map
     * @return Map from variable name to its value
     * @throws IOException
     */
    static Map<String, Double> getVariableValues(Path variablesPath) throws IOException {
        log.info("Reading variable values into a map");
        Properties properties = new Properties();
        properties.load(new FileReader(variablesPath.toFile()));
        Map<String, Double> values = new HashMap<>();
        for(Map.Entry<Object, Object> e : properties.entrySet()) {
            values.put(e.getKey().toString(), Double.parseDouble(e.getValue().toString()));
        }
        return values;
    }

    private LinearEquation createLinearEquation(SortedMap<String, Double> initValues, Map<Long, MyDouble> aggregatedErrorByWindow) {
        Map<String, Integer> indexMap = new HashMap<>();
        int idx= 0;
        for(String name: initValues.keySet()) {
            indexMap.put(name, idx++);
        }

        LinearEquation linearEquation = new LinearEquation(indexMap);

        aggregatedErrorByWindow.values().stream()
                .filter(md -> md.toExpression() instanceof Addition) // We can just skip constant terms from minimization
                .map(md -> (Addition) md.toExpression())
                .forEach(linearEquation::addRow);

        return linearEquation;
    }

    /**
     * Generates a python code that minimizes least square error
     * @param initValues SortedMap of variable names to their values. This is initial stored power model
     * @return File containing the generated python code
     * @throws IOException
     */
    private File generatePyCode(SortedMap<String, Double> initValues, LinearEquation linearEquation) throws IOException {
        Path outDir = bridge.getOutDir();
        File pyFile = outDir.resolve("minlsq.py").toFile();
        pyFile.createNewFile();
        pyFile.setExecutable(true);
        try(FileWriter writer = new FileWriter(pyFile)) {
            writer.append("#!/usr/local/bin/python2\n" +
                    "import sys\n" +
                    "from scipy.optimize import lsq_linear\n" +
                    "from scipy.sparse import coo_matrix\n" +
                    "import numpy as np\n");
            int idx = 0;

            for(String var : initValues.keySet())
                writer.append("\nidx_" + var + "=" + idx++);
            writer.append("\nidx_LAST=" + initValues.size() + "\n\n");

            linearEquation.write(writer);

            writer.append("\n\ndef main():\n" +
                    "    x0 = np.array([None] * idx_LAST)\n");

            writer.append(initValues.entrySet().parallelStream()
                    .map(e -> "    x0[idx_"+ e.getKey() +"] = " + e.getValue()).collect(joining("\n")));

            writer.append("\n\n    lb = [10] * idx_LAST\n" +
                    "    for b in range(idx_LAST) :\n" +
                    "        lb[b] = 0.7 * x0[b]     # plus/minus 30% of original values\n" +
                    "\n" +
                    "    ub = [10] * idx_LAST\n" +
                    "    for b in range(idx_LAST):\n" +
                    "        ub[b] = 1.3 * x0[b]  # plus/minus 30% of original values\n");

            writer.append("    res = lsq_linear(lhsMatrix, rhsVector, bounds=(lb, ub), lsmr_tol='auto', max_iter=100000)\n");

            writer.append("    print (res, file=sys.stderr)\n" +
                    "    if not res.success :\n" +
                    "        print(\"Failed to optimize\", file=sys.stderr)\n");

            writer.append("    x=res.x\n");
            writer.append(initValues.keySet().parallelStream().map(k -> "    print('" + k + " =', x[idx_" + k + "])\n")
                    .collect(joining()));

            writer.append("\nmain()");
        }
        log.info("Dumped python file to {}", pyFile.toPath().toString());
        return pyFile;
    }

//    public static void main(String[] args) throws IOException, InterruptedException {
//        Optimizer pyCode = new Optimizer();
//        pyCode.optimizeAndRewrite(new TreeMap<String, Double>() {{
//            put("a", 3.0);
//        }}, errorEvents);
//    }
}
