package com.mobileenerlytics.symbolic.finishers;

import com.mobileenerlytics.symbolic.expression.Addition;
import com.mobileenerlytics.symbolic.expression.Term;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

/**
 * Holds an equation of the form A x = b. Here A is a
 * <a href="https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.coo_matrix.html#scipy.sparse.coo_matrix">sparse matrix</a>
 * and b is a vector. This equation will be passed to solver for solving for `x` which will be the power model that
 * minimizes least square error.
 *
 * Since `A` is a sparse matrix, it's held in three arrays {@link #data}, {@link #colIdx} and {@link #rowIdx}. {@link #rhsVector}
 * is just an array.
 */
class LinearEquation {
    private PythonArray<Integer> rowIdx, colIdx;
    private PythonArray<Double> data, rhsVector;
    private int row = 0;
    private final Map<String, Integer> indexMap;

    LinearEquation(Map<String, Integer> indexMap) {
        this.indexMap = indexMap;

        rowIdx = new PythonArray<>();
        colIdx = new PythonArray<>();
        data = new PythonArray<>();
        rhsVector = new PythonArray<>();
        row = 0;
    }

    void addRow(Addition a) {
        boolean writtenToRhs = false;
        for(Term t : a.terms.values()) {
            if(t.name == Term.CONSTANT) {
                // Append constant into the right hand side rhsVector
                // Multiply by -1 since we're taking it into RHS
                rhsVector.add(-1 * t.value);
                writtenToRhs = true;
                continue;
            }

            data.add(t.value);
            colIdx.add(indexMap.get(t.name));
            rowIdx.add(row);
        }
        row++;

        if(!writtenToRhs)
            rhsVector.add(0.0);
    }

    void write(Writer writer) throws IOException {
        writer.append(String.format("row = %s\n", rowIdx.toString()));
        writer.append(String.format("col = %s\n", colIdx.toString()));
        writer.append(String.format("data = %s\n", data.toString()));
        writer.append(String.format("lhsMatrix = coo_matrix((data, (row, col)), shape=(%d, %d))\n", row, indexMap.size()));

        writer.append(String.format("rhsVector = %s\n", rhsVector.toString()));
    }
}
