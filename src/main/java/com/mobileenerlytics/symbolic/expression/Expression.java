package com.mobileenerlytics.symbolic.expression;

import com.google.common.base.Verify;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public abstract class Expression {
    private static Map<String, Term> cloneMap(Map<String, Term> terms) {
        return terms.values().stream().map(t -> new Term(t)).collect(toMap(t -> t.name, identity()));
    }

    private static void addTermToTermMap(Term term, Map<String, Term> terms) {
        Term newTerm;
        if(terms.containsKey(term.name))
            newTerm = term.add(terms.get(term.name));
        else
            newTerm = new Term(term);

        if(newTerm.isZero())
            terms.remove(term.name);
        else
            terms.put(term.name, newTerm);
    }

    private static Expression expressionFromTermMap(Map<String, Term> terms) {
       if(terms.isEmpty())
            return new Term(0, Term.CONSTANT);
        if(terms.size() == 1)
            return terms.values().stream().findAny().get();

        return new Addition(terms);
    }

    private static Expression addTermToAddition(Term term, Addition addition) {
        Map<String, Term> terms = cloneMap(addition.terms);
        addTermToTermMap(term, terms);
        return expressionFromTermMap(terms);
    }

    private static Expression addAdditionToAddition(Addition a, Addition b) {
        Map<String, Term> terms = cloneMap(a.terms);
        b.terms.values().forEach(t -> {
            addTermToTermMap(t, terms);
        });
        return expressionFromTermMap(terms);
    }

    public static Expression add(Expression left, Expression right) {
        if(left instanceof Addition && right instanceof Addition)
            return addAdditionToAddition((Addition) left, (Addition) right);

        if(left instanceof Addition)
            return addTermToAddition((Term) right, (Addition) left);

        if(right instanceof Addition)
            return addTermToAddition((Term) left, (Addition) right);

        Term leftTerm = (Term) left;
        Term rightTerm = (Term) right;

        if(leftTerm.name.equals(rightTerm.name))
            return new Term(leftTerm.value + rightTerm.value, leftTerm.name);

        if(leftTerm.isZero())
            return rightTerm;

        if(rightTerm.isZero())
            return leftTerm;

        return new Addition(leftTerm, rightTerm);
    }

    public static Expression subtract(Expression left, Expression right) {
        return add(left, right.multiply(-1));
    }

    public static Expression multiply(Expression left, Expression right) {
        Verify.verify(left.isNumber() || right.isNumber());
        if(left.isNumber()) {
            Term leftTerm = (Term) left;
            return right.multiply(leftTerm.value);
        }
        Term rightTerm = (Term) right;
        return left.multiply(rightTerm.value);
    }

    public static Expression clone(Expression expression) {
        if(expression instanceof Term)
            return new Term((Term) expression);
        return new Addition(cloneMap(((Addition) expression).terms));
    }


    public boolean isNumber() {
        return false;
    }

    public abstract Expression multiply(double v);

    static final Pattern pattern = Pattern.compile("\\((-?\\d+\\.\\d+)( \\* ([^ ]*))?\\)");
    public static double evaluate(String s, Map<String, Double> values) {
        Matcher matcher = pattern.matcher(s);
        double value = 0;
        while(matcher.find()){
            if(matcher.group(3) != null)    // Term with variable
                value += Double.parseDouble(matcher.group(1)) * values.get(matcher.group(3));
            else    // Constant
                value += Double.parseDouble(matcher.group(1));
        }
        return value;
    }
}
