package com.mobileenerlytics.symbolic.expression;

import com.google.common.base.Verify;

public class Term extends Expression {
    public static final String CONSTANT = "zzzz_CONSTANT";   // Make sure it comes at the end
    public final double value;
    public final String name;

    public Term(double value, String name) {
        this.value = value;
        this.name = name;

    }

    public Term(Term a) {
        this(a.value, a.name);
    }

    /**
     * Returns 'true' if this node represents a number, and 'false' otherwise.
     */
    @Override
    public boolean isNumber() {
        return name.equals(CONSTANT);
    }

    public boolean isZero() {
        return Math.abs(value) <= 0.0001;
    }

    @Override
    public Expression multiply(double v) {
        return new Term(value * v, name);
    }

    public Term add(Term term) {
        Verify.verify(name.equals(term.name));
        return new Term(value + term.value, name);
    }

    @Override
    public String toString() {
        if(isNumber())
            return String.format("(%.4f)", value);
        return String.format("(%.4f * %s)", value, name);
    }

    @Override
    public boolean equals(Object o) {
        if (! (o instanceof Term))
            return false;
        Term t = (Term) o;
        if(!name.equals(t.name))
            return false;
        return Math.abs(value - t.value) < 0.0001;
    }
}
