package com.mobileenerlytics.symbolic.expression;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.FileSystemUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;

@Component
public class SymbolicBridge {
    @Autowired
    Environment environment;

    @Value("${parent.dir:/tmp/symbolic/}")
    public String parentPath;

    Path outDir, concreteOutDir;

    @PostConstruct
    public void init() {
        Path parent = FileSystems.getDefault().getPath(parentPath);
        File parentFile = parent.toFile();
        parentFile.mkdirs();

        if(environment.acceptsProfiles("symbolic")) {
            outDir = parent.resolve("eprof.files.sym");
            FileSystemUtils.deleteRecursively(outDir.toFile());
            outDir.toFile().mkdirs();
        }

        concreteOutDir = parent.resolve("eprof.files");
        FileSystemUtils.deleteRecursively(concreteOutDir.toFile());
        concreteOutDir.toFile().mkdirs();
    }

    public Path getOutDir() {
        if(environment.acceptsProfiles("symbolic"))
            return outDir;
        return concreteOutDir;
    }
    public Path getConcreteOutDir() {
        return concreteOutDir;
    }
}
