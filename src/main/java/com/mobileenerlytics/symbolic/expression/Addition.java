package com.mobileenerlytics.symbolic.expression;

import com.google.common.base.Preconditions;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toMap;

public class Addition extends Expression {
    // variable name -> term
    public final Map<String, Term> terms;

    public Addition(Term a, Term b) {
        Preconditions.checkArgument(!a.name.equals(b.name));
        Preconditions.checkArgument(!a.isZero());
        Preconditions.checkArgument(!b.isZero());
        this.terms = Collections.unmodifiableMap(new HashMap<String, Term>() {{
            put(a.name, new Term(a));
            put(b.name, new Term(b));
        }});
    }

    public Addition(Map<String, Term> terms) {
        this.terms = Collections.unmodifiableMap(terms);
    }

    @Override
    public Expression multiply(double v) {
        if(v == 0)
            return new Term(0, Term.CONSTANT);
        return new Addition(this.terms.values().stream()
                .map(t -> (Term) t.multiply(v)).collect(toMap(t -> t.name, Function.identity())));
    }

    @Override
    public String toString() {
        return terms.values().stream()
                .sorted(comparing(t -> t.name))
                .map(Term::toString)
                .collect(Collectors.joining(" + "));
    }

    @Override
    public boolean equals(Object o) {
        if(! (o instanceof Addition))
            return false;
        Addition a = (Addition) o;
        return a.toString().equals(toString());
    }
}
