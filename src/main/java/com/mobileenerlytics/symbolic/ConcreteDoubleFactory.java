package com.mobileenerlytics.symbolic;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
@Profile("!symbolic")
public class ConcreteDoubleFactory implements IDoubleFactory {
    @Override
    public MyDouble newSymbolicDouble(double value) {
        return new ConcreteDouble(value);
    }

    @Override
    public MyDouble newSymbolicUnmodifiableDouble(double value, String variableName) {
        return new ConcreteUnmodifiableDouble(value);
    }

    @Override
    public MyDouble newSymbolicUnmodifiableDouble(MyDouble myDouble) {
        return new ConcreteUnmodifiableDouble(myDouble);
    }

    @Override
    public void finish(List<? extends IEvent<MyDouble>> errorEvents) throws IOException, InterruptedException {

    }
}
