package com.mobileenerlytics.symbolic;

import com.mobileenerlytics.symbolic.expression.SymbolicBridge;
import com.mobileenerlytics.symbolic.expression.Term;
import com.mobileenerlytics.symbolic.finishers.Optimizer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import static com.mobileenerlytics.symbolic.expression.Term.CONSTANT;

@Slf4j
@Component
@Profile("symbolic")
public class SymbolicDoubleFactory implements IDoubleFactory {
    final SortedMap<String, Double> initValues = new TreeMap<>();

    @Autowired
    private Optimizer optimizer;

    @Override
    public MyDouble newSymbolicDouble(double value) {
        return new SymbolicDouble(value, Term.CONSTANT);
    }

    @Override
    public MyDouble newSymbolicUnmodifiableDouble(double value, String variableName) {
        if(variableName == null) {
            variableName = CONSTANT;
            return new SymbolicUnmodifiableDouble(value, variableName);
        }
        initValues.put(variableName, value);
        return new SymbolicUnmodifiableDouble(1, variableName);
    }

    @Override
    public MyDouble newSymbolicUnmodifiableDouble(MyDouble myDouble) {
        return new SymbolicUnmodifiableDouble(myDouble);
    }

    @Override
    public void finish(List<? extends IEvent<MyDouble>> errorEvents) throws IOException, InterruptedException {
        optimizer.optimizeAndRewrite(initValues, errorEvents);
    }
}
