package com.mobileenerlytics.symbolic;

public interface IEvent<T> {
    long getTimeUs();
    T getValue();
}
