package com.mobileenerlytics.symbolic;

import com.google.common.annotations.VisibleForTesting;
import com.mobileenerlytics.symbolic.expression.Expression;
import com.mobileenerlytics.symbolic.expression.Term;

import static com.mobileenerlytics.symbolic.DoubleUtil.hasValue;
import static com.mobileenerlytics.symbolic.DoubleUtil.value;

@VisibleForTesting
public class ConcreteUnmodifiableDouble extends MyDouble {
    final double value;
    private Expression expression;
    public ConcreteUnmodifiableDouble(double value) {
        this.value = value;
    }

    public ConcreteUnmodifiableDouble(MyDouble myDouble) {
        value = DoubleUtil.value(myDouble);
    }

    @Override
    public MyDouble add(MyDouble md) {
        if(hasValue(md))
            return new ConcreteUnmodifiableDouble(value + value(md));
        return new SymbolicUnmodifiableDouble(value, Term.CONSTANT).add(md);
    }

    @Override
    public MyDouble subtract(MyDouble md) {
        if(hasValue(md))
            return new ConcreteUnmodifiableDouble(value - value(md));
        return new SymbolicUnmodifiableDouble(value, Term.CONSTANT).subtract(md);
    }

    @Override
    public MyDouble multiply(MyDouble md) {
        return new ConcreteUnmodifiableDouble(value * value(md));
    }

    @Override
    public MyDouble multiply(double value) {
        return new ConcreteUnmodifiableDouble(this.value * value);
    }

    @Override
    public MyDouble divide(double value) {
        return new ConcreteUnmodifiableDouble(this.value / value);
    }

    @Override
    public Expression toExpression() {
        if(expression == null)
            expression = new Term(value, Term.CONSTANT);
        return expression;
    }

    @Override
    public boolean isUnmodifiable() {
        return true;
    }

    @Override
    public String toString() {
        return String.format("%.2f", value);
    }
}
