package com.mobileenerlytics.symbolic;

import com.mobileenerlytics.symbolic.expression.Expression;
import com.mobileenerlytics.symbolic.expression.Term;

public class ConcreteDouble extends MyDouble {
    double value;
    Expression expression;
    public ConcreteDouble(double value) {
        this.value = value;
    }

    public ConcreteDouble(MyDouble md) {
        this.value = DoubleUtil.value(md);
    }

    @Override
    public MyDouble add(MyDouble md) {
        value += DoubleUtil.value(md);
        return this;
    }

    @Override
    public MyDouble subtract(MyDouble md) {
        value -= DoubleUtil.value(md);
        return this;
    }

    @Override
    public MyDouble multiply(MyDouble md) {
        value *= DoubleUtil.value(md);
        return this;
    }

    @Override
    public MyDouble multiply(double value) {
        this.value *= value;
        return this;
    }

    @Override
    public MyDouble divide(double value) {
        this.value /= value;
        return this;
    }

    @Override
    public Expression toExpression() {
        if(expression == null)
            expression = new Term(value, Term.CONSTANT);
        return expression;
    }

    @Override
    public String toString() {
        return String.format("%.2f", value);
    }
}
