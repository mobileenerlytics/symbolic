package com.mobileenerlytics.symbolic;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Verify;
import com.mobileenerlytics.symbolic.expression.Term;

@VisibleForTesting
public abstract class DoubleUtil {
    public static boolean hasValue(MyDouble myDouble) {
        return myDouble.toExpression().isNumber();
    }

    public static double value(MyDouble myDouble) {
        Verify.verify(hasValue(myDouble));
        return ((Term) myDouble.toExpression()).value;
    }

}
