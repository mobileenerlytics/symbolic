package com.mobileenerlytics.symbolic;

import java.io.IOException;
import java.util.List;

public interface IDoubleFactory {
    MyDouble newSymbolicDouble(double value);

    MyDouble newSymbolicUnmodifiableDouble(double value, String variableName);

    MyDouble newSymbolicUnmodifiableDouble(MyDouble myDouble);

    void finish(List<? extends IEvent<MyDouble>> errorEvents) throws IOException, InterruptedException;
}
