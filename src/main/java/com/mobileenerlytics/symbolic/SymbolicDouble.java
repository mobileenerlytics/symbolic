package com.mobileenerlytics.symbolic;

import com.mobileenerlytics.symbolic.expression.Expression;
import com.mobileenerlytics.symbolic.expression.Term;

class SymbolicDouble extends MyDouble {
    private Expression expression;
    public SymbolicDouble(final double value, String variableName) {
        expression = new Term(value, variableName);
    }

    public SymbolicDouble(final MyDouble myDouble) {
        expression = myDouble.toExpression();
    }

    @Override
    public MyDouble add(final MyDouble operand) {
        expression = Expression.add(expression, operand.toExpression());
        return this;
    }

    @Override
    public MyDouble subtract(final MyDouble operand) {
        expression = Expression.subtract(expression, operand.toExpression());
        return this;
    }

    @Override
    public MyDouble multiply(MyDouble md) {
        expression = Expression.multiply(expression, md.toExpression());
        return this;
    }

    @Override
    public MyDouble multiply(double value) {
        expression = expression.multiply(value);
        return this;
    }

    @Override
    public MyDouble divide(double value) {
        return multiply(1.0/value);
    }

    @Override
    public int hashCode() {
        throw new RuntimeException();
    }

    @Override
    public Expression toExpression() {
        return expression;
    }

    @Override
    public String toString() {
        return expression.toString();
    }
}
