package com.mobileenerlytics.symbolic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class DoubleFactory {
    @Autowired
    IDoubleFactory factory;

    @Value("${variable.prefixes:}")
    String[] prefixes = new String[0];

    public static MyDouble newDouble(double value) {
        return new ConcreteDouble(value);
    }

    public static MyDouble newUnmodifiableDouble(double value) {
        return new ConcreteUnmodifiableDouble(value);
    }

    public MyDouble newSymbolicDouble(double value) {
        return factory.newSymbolicDouble(value);
    }

    public MyDouble cloneSymbolicUnmodifiableDouble(MyDouble myDouble) {
        return factory.newSymbolicUnmodifiableDouble(myDouble);
    }

    public MyDouble newSymbolicUnmodifiableDouble(double value, @NonNull String variableName) {
        if(variableName != null)
        for(String prefix: prefixes)
            if(variableName.startsWith(prefix))
                return factory.newSymbolicUnmodifiableDouble(value, variableName);
        return new ConcreteUnmodifiableDouble(value);
    }

    public void finish(List<? extends IEvent<MyDouble>> errorEvents) throws IOException, InterruptedException {
        factory.finish(errorEvents);
    }
}
